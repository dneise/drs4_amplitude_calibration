import h5py
import numpy as np
from scipy import stats
import time

my_h5_file = h5py.File("database.h5",'r')
linear_fit_result = h5py.File("trainingresult_no_shuffle.h5")

# 0|slope, 1|intercept, 2|r_value, 3|p_value, 4|std_err
bsl_fit_no_shuffle = np.zeros( (5, 1440, 1024, 5), dtype=np.float32)
gain_fit_no_shuffle = np.zeros( (5, 1440, 1024, 5), dtype=np.float32)
trioff_fit_no_shuffle = np.zeros((5, 1440, 300, 5), dtype=np.float32)

###cross-validation 
#resampling: 5-folds
DRS4_chip_temperature = my_h5_file.get("DRS4_chip_temperature")[:, :2700]
DRS4_chip_temperature_160 = DRS4_chip_temperature[np.arange(0,1440,9),:]# (1440, 2700) to (160, 2700)
temp_folds = np.hsplit(DRS4_chip_temperature_160, 5) #array(5,160,540)

for training_id in range(5):
    temp_train = list(temp_folds) #list(5,160,540)
    temp_test = temp_train.pop(training_id) #(160,540)
    temp_train = np.concatenate(temp_train, axis=1) #(160,2160)
    Tmask = temp_train[...] > 5 #(160,2160)

    read_chunk_size = 9 

    for pixel_chunk_id in range(1440/read_chunk_size):
        mask = Tmask[pixel_chunk_id] #(1,2160)
        temp_train_with_mask = temp_train[pixel_chunk_id, mask] #(1,2160)
    
        aBaselineMean = my_h5_file.get("BaselineMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size, :, :2700]#(9,1024,2700)
        bsl_folds = np.dsplit(aBaselineMean, 5) #(5, 9, 1024, 540)
        bsl_train = list(bsl_folds) #list(5, 9, 1024, 540)
        bsl_test = bsl_train.pop(training_id) #(9, 1024, 540)
        bsl_train = np.concatenate(bsl_train, axis=2) #(9,1024,2160)
        bsl_chunk_with_mask = bsl_train[:, :, mask] #(9,1024,2160) # ~ 240ms + ~1.2s
    
        aGainMean = my_h5_file.get("GainMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size, :, :2700]#(9,1024,2700)
        gain_folds = np.dsplit(aGainMean, 5) #(5, 9, 1024, 540)
        gain_train = list(gain_folds) #list(5, 9, 1024, 540)
        gain_test = gain_train.pop(training_id) #(9, 1024, 540)
        gain_train = np.concatenate(gain_train, axis=2) #(9,1024,2160)
        gain_chunk_with_mask = gain_train[:, :, mask] #(9,1024,2160) # ~ 240ms + ~1.2s

        aTriggerOffsetMean = my_h5_file.get("TriggerOffsetMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size, :, :2700]#(9,300,2700)
        trioff_folds = np.dsplit(aTriggerOffsetMean, 5) #(5, 9, 300, 540)
        trioff_train = list(trioff_folds) #list(5, 9, 300, 540)
        trioff_test = trioff_train.pop(training_id) #(9, 300, 540)
        trioff_train = np.concatenate(trioff_train, axis=2) #(9,300,2160)
        trioff_chunk_with_mask = trioff_train[:, :, mask] #(9,300,2160)
        
        for pixel_in_chunk_id in range(read_chunk_size):
            pixel_id = pixel_chunk_id * read_chunk_size + pixel_in_chunk_id
            print time.asctime(), "I am working on training_id: ", training_id, " pixel:", pixel_id

            for j in range(0,1024):
                # fitting 5000 points takes about 250us
                bsl_fit_no_shuffle[training_id, pixel_id, j, :] = stats.linregress(temp_train_with_mask[...], bsl_chunk_with_mask[pixel_in_chunk_id, j, :])
                gain_fit_no_shuffle[training_id, pixel_id, j, :] = stats.linregress(temp_train_with_mask[...], gain_chunk_with_mask[pixel_in_chunk_id, j, :])
            for k in range(0,300):
                trioff_fit_no_shuffle[training_id, pixel_id, k, :] = stats.linregress(temp_train_with_mask[...], trioff_chunk_with_mask[pixel_in_chunk_id, k, :])

bsl_temp_no_shuffle = linear_fit_result.create_dataset(
    name="bsl_temp_no_shuffle",
    data=bsl_fit_no_shuffle) #(5, 1440, 1024, 5)

gain_temp_no_shuffle = linear_fit_result.create_dataset(
    name="gain_temp_no_shuffle",
    data=gain_fit_no_shuffle) #(5, 1440, 1024, 5)

trioff_temp_no_shuffle = linear_fit_result.create_dataset(
    name="trioff_temp_no_shuffle",
    data=trioff_fit_no_shuffle) #(5, 1440, 300, 5) 

linear_fit_result.close()

my_h5_file.close()
