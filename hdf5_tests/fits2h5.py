""" reading all drs.fits files, getting their start-stop time
reading the according Temperature from the slow data DB.
pushing all that into a large chunked H5 file, so one can work on that.

"""
import h5py
import glob
from astropy.io import fits
from fact.time import fjd, iso2dt
from fact.slowdata import connect
import time

N_pixel = 1440
__roi_long__ = 1024
__roi_short__ = 300

fits_file_paths = sorted(glob.glob("/scratch_loc/fact_drs.fits/*"))
N_files = len(fits_file_paths)

some_stuff = [
    ("Baseline_Starttime", "RUN0-BEG"),
    ("Baseline_Stoptime", "RUN0-END"),
    ("Gain_Starttime", "RUN1-BEG"),
    ("Gain_Stoptime", "RUN1-END"),
    ("TriggerOffset_Starttime", "RUN2-BEG"),
    ("TriggerOffset_Stoptime", "RUN2-END"),
    ]

other_stuff = [
    ("BaselineMean", __roi_long__),
    ("BaselineRms", __roi_long__),
    ("GainMean", __roi_long__),
    ("GainRms", __roi_long__),
    ("TriggerOffsetMean", __roi_short__),
    ("TriggerOffsetRms", __roi_short__),
    ]

def create_dataset(h5file, name, roi):
    return h5file.create_dataset(
        name=name,
        shape=(N_pixel, roi, N_files),
        chunks=(18, 16, 30),
        maxshape=(N_pixel, roi, None),
        dtype='float32')

my_h5_file = h5py.File("database.h5")
for x in other_stuff:
    create_dataset(my_h5_file, x[0], x[1])

for x in some_stuff:
    my_h5_file.create_dataset(
        name=x[0],
        shape=(N_files, ),
        maxshape=(None, ),
        dtype='float64')


# Temperature
DRS4_chip_temperature = my_h5_file.create_dataset(
    name="DRS4_chip_temperature",
    shape=(N_pixel, N_files),
    maxshape=(N_pixel, None),
    dtype='float32')

# Exceptions
list_of_exception = []

db = connect()

for file_id, filename in enumerate(fits_file_paths):
    print time.asctime(), "working on file:", filename

    fits_file = fits.open(filename)

    # Write in Baseline/Gain/TriggerOffset time in dataset
    for x in some_stuff:
        my_h5_file[x[0]][file_id] = fjd(iso2dt(fits_file[1].header[x[1]]))

    # Write in temp in dataset
    start_time = my_h5_file["Baseline_Starttime"][file_id]
    stop_time = my_h5_file["TriggerOffset_Stoptime"][file_id]
    try:
        Temperature = db.FAD_CONTROL_TEMPERATURE.from_until(
            start_time,
            stop_time)
        average_chip_temperature = Temperature["temp"].mean(axis=0)
        average_channel_temperature = average_chip_temperature.repeat(9)
        DRS4_chip_temperature[:, file_id] = average_channel_temperature
    except IndexError:
        print 'file ID: ', file_id, '/file name:', filename
        list_of_exception.append(file_id)

    for x in other_stuff:
        dummy = fits_file[1].data[x[0]].reshape(N_pixel, x[1])
        my_h5_file[x[0]][:, :, file_id] = dummy

    fits_file.close()

Exception_File_ID = my_h5_file.create_dataset(
    name="Exception_File_ID",
    data=list_of_exception)

my_h5_file.close()
