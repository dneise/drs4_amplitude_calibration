import h5py
import numpy as np
import time

training = h5py.File("trainingresult_with_shuffle3.h5")
database = h5py.File("database.h5",'r')

if "deviation" in training.keys():
    deviation = training.get("deviation")
else:
    deviation = training.create_dataset(
        name="deviation",
        shape=(3, 5, 1440, 1024, 540), #(u,training_id, pixel_id, cell_id, test_size)
        chunks=(3, 5, 18, 16, 20),
        dtype='float32')

bsl_slope = training.get("bsl_temp_with_shuffle")[:, :, :, 0] #(5, 1440, 1024, 1)
gain_slope = training.get("gain_temp_with_shuffle")[:, :, :, 0] #(5, 1440, 1024, 1)

bsl_intercept = training.get("bsl_temp_with_shuffle")[:, :, :, 1] #(5, 1440, 1024, 1)
gain_intercept = training.get("gain_temp_with_shuffle")[:, :, :, 1] #(5, 1440, 1024, 1)

test_id = training.get("test_id") #(5,540)

DRS4_chip_temperature = database.get("DRS4_chip_temperature")[:,:2700] #(1440, 2700)
DRS4_chip_temperature_160 = DRS4_chip_temperature[np.arange(0,1440,9),:]# (1440, 2700) to (160, 2700)

read_chunk_size = 9

for l in range(3):
     
  u_initial = 10**(l+1)

  for training_id in range(5):
     
    temp_test = DRS4_chip_temperature_160[:,test_id[training_id, :]] #(160,540)

    for pixel_chunk_id in range(1440/read_chunk_size):
    
      bsl_measured = database.get("BaselineMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size,:,:2700] #(9, 1024, 2700)
      bsl_test = bsl_measured[:, :, test_id[training_id, :]] #(9, 1024, 540)

      gain_measured = database.get("GainMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size,:,:2700] #(9, 1024, 2700)
      gain_test = gain_measured[:, :, test_id[training_id, :]] #(9, 1024, 540)
          
      for pixel_in_chunk_id in range(read_chunk_size):
            
        pixel_id = pixel_chunk_id * read_chunk_size + pixel_in_chunk_id
        print time.asctime(), " u_initial = ", u_initial, " training_id: ", training_id, " pixel:", pixel_id

        for j in range(1024):# j (1024) cell_id
        
          a = np.zeros( (540), dtype=np.float32) #ADC voltage
          u = np.zeros( (540), dtype=np.float32) #voltage

          bsl_predicted = bsl_intercept[training_id, pixel_id, j] + bsl_slope[training_id, pixel_id, j] * temp_test[pixel_chunk_id, :]#(540)
          gain_predicted = gain_intercept[training_id, pixel_id, j] + gain_slope[training_id, pixel_id, j] * temp_test[pixel_chunk_id, :]#(540)
                
          #u[pixel_id,j,k] to a[pixel_id,j,k] by using gain_measured and baseline_measured
          a = (u_initial * gain_test[pixel_in_chunk_id,j,:] /1907.35 + bsl_test[pixel_in_chunk_id,j,:]) * 2000. / 4096. #ADC voltage #(540)
          #a[pixel_id,j,k] to u[pixel_id,j,k] by using gain_predicted and baseline_predicted
          u = (a * 4096. / 2000. - bsl_predicted) * 1907.35 / gain_predicted #(540)
                  
          deviation[l, training_id, pixel_id, j, :] = u / u_initial -1 #(540)

training.close()
database.close()
