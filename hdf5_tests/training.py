import h5py
import numpy as np
from scipy import stats
import time

my_h5_file = h5py.File("database.h5",'r')
linear_fit_result = h5py.File("trainingresult.h5")

# 0|slope, 1|intercept, 2|r_value, 3|p_value, 4|std_err
bsl_fit = np.zeros( (1440, 1024, 5), dtype=np.float32)
gain_fit = np.zeros( (1440, 1024, 5), dtype=np.float32)
triggeroffset_fit = np.zeros( (1440, 300, 5), dtype=np.float32)

DRS4_chip_temperature = my_h5_file.get("DRS4_chip_temperature")[:, :2700]

# aDRS4_chip_temperature.shape is (1440, 2700), I reduce it here to (160, 2700)
DRS4_chip_temperature_160 = DRS4_chip_temperature[np.arange(0,1440,9),:]

Tmask = DRS4_chip_temperature_160[...] > 5

read_chunk_size = 9

for pixel_chunk_id in range(1440/read_chunk_size):
    mask = Tmask[pixel_chunk_id]
    DRS4_chip_temperature_160_with_mask = DRS4_chip_temperature_160[pixel_chunk_id, mask]
    
    aBaselineMean = my_h5_file.get("BaselineMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size, :, :2700]
    baseline_for_chunk_with_mask = aBaselineMean[:, :, mask] # ~ 240ms + ~1.2s
    
    aGainMean = my_h5_file.get("GainMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size, :, :2700]
    gain_for_chunk_with_mask = aGainMean[:, :, mask] # ~ 240ms + ~1.2s

    aTriggerOffsetMean = my_h5_file.get("TriggerOffsetMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size, :, :2700]
    triggeroffset_for_chunk_with_mask = aTriggerOffsetMean[:, :, mask] # ~ 240ms + ~1.2s #(9,300,2700)
    
    for pixel_in_chunk_id in range(read_chunk_size):
        pixel_id = pixel_chunk_id * read_chunk_size + pixel_in_chunk_id
        print time.asctime(), "I am working on pixel:", pixel_id

        for j in range(0,1024):
            #fitting 5000 points takes about 250us
            bsl_fit[pixel_id, j, :] = stats.linregress(DRS4_chip_temperature_160_with_mask[...], baseline_for_chunk_with_mask[pixel_in_chunk_id, j, :])
            gain_fit[pixel_id, j, :] = stats.linregress(DRS4_chip_temperature_160_with_mask[...], gain_for_chunk_with_mask[pixel_in_chunk_id, j, :])
        for k in range(0,300):
            triggeroffset_fit[pixel_id, k, :] = stats.linregress(DRS4_chip_temperature_160_with_mask[...], triggeroffset_for_chunk_with_mask[pixel_in_chunk_id, k, :])


bsl_temp = linear_fit_result.create_dataset(
    name="bsl_temp",
    data=bsl_fit)

gain_temp = linear_fit_result.create_dataset(
    name="gain_temp",
    data=gain_fit)

triggeroffset_temp = linear_fit_result.create_dataset(
    name="triggeroffset_temp",
    data=triggeroffset_fit)

linear_fit_result.close()

my_h5_file.close()
