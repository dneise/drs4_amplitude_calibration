import h5py
import numpy as np
from scipy import stats

my_h5_file = h5py.File("database.h5",'r+')


# 0|slope, 1|intercept, 2|r_value, 3|p_value, 4|std_err
if "BaselineMean_temp" in my_h5_file.keys():
    BaselineMean_temp = my_h5_file.get("BaselineMean_temp")
else:
    BaselineMean_temp = my_h5_file.create_dataset(
        name="BaselineMean_temp",
        shape=(1440, 1024, 5),
        maxshape=(1440, 1024, None),
        dtype='float32')

# if "BaselineRms_temp" in my_h5_file.keys():
#     BaselineRms_temp = my_h5_file.get("BaselineRms_temp")
# else:
#     BaselineRms_temp = my_h5_file.create_dataset(
#         name="BaselineRms_temp",
#         shape=(1440, 1024, 5),
#         maxshape=(1440, 1024, None),
#         dtype='float32')

if "GainMean_temp" in my_h5_file.keys():
    GainMean_temp = my_h5_file.get("GainMean_temp")
else:
    GainMean_temp = my_h5_file.create_dataset(
        name="GainMean_temp",
        shape=(1440, 1024, 5),
        maxshape=(1440, 1024, None),
        dtype='float32')

# if "GainRms_temp" in my_h5_file.keys():
#     GainRms_temp = my_h5_file.get("GainRms_temp")
# else:
#     GainRms_temp = my_h5_file.create_dataset(
#         name="GainRms_temp",
#         shape=(1440, 1024, 5),
#         maxshape=(1440, 1024, None),
#         dtype='float32')

if "TriggerOffsetMean_temp" in my_h5_file.keys():
    TriggerOffsetMean_temp = my_h5_file.get("TriggerOffsetMean_temp")
else:
    TriggerOffsetMean_temp = my_h5_file.create_dataset(
        name="TriggerOffsetMean_temp",
        shape=(1440, 300, 5),
        maxshape=(1440, 300, None),
        dtype='float32',)

# if "TriggerOffsetRms_temp" in my_h5_file.keys():
#     TriggerOffsetRms_temp = my_h5_file.get("TriggerOffsetRms_temp")
# else:
#     TriggerOffsetRms_temp = my_h5_file.create_dataset(
#         name="TriggerOffsetRms_temp",
#         shape=(1440, 300, 5),
#         maxshape=(1440, 300, None),
#         dtype='float32')

aBaselineMean = my_h5_file.get("BaselineMean")
aBaselineRms = my_h5_file.get("BaselineRms")
aGainMean = my_h5_file.get("GainMean")
aGainRms = my_h5_file.get("GainRms")
aTriggerOffsetMean = my_h5_file.get("TriggerOffsetMean")
aTriggerOffsetRms = my_h5_file.get("TriggerOffsetRms")

for i in range(0,1440):
    Tmask = DRS4_chip_temperature[i, :] > 5
    for j in range(0,1024):
        BaselineMean_temp[i, j, :] = stats.linregress(DRS4_chip_temperature[i, :][Tmask], aBaselineMean[i, j, :][Tmask])
        BaselineRms_temp[i, j, :] = stats.linregress(DRS4_chip_temperature[i, :][Tmask], aBaselineRms[i, j, :][Tmask])
        GainMean_temp[i, j, :] = stats.linregress(DRS4_chip_temperature[i, :][Tmask], aGainMean[i, j, :][Tmask])
        GainRms_temp[i, j, :] = stats.linregress(DRS4_chip_temperature[i, :][Tmask], aGainRms[i, j, :][Tmask])
    for k in range(0,300):
        TriggerOffsetMean_temp[i, k, :] = stats.linregress(DRS4_chip_temperature[i, :][Tmask], aTriggerOffsetMean[i, k, :][Tmask])
        TriggerOffsetRms_temp[i, k, :] = stats.linregress(DRS4_chip_temperature[i, :][Tmask], aTriggerOffsetRms[i, k, :][Tmask])

my_h5_file.close()
