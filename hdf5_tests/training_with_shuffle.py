import h5py
import numpy as np
from scipy import stats
import time

my_h5_file = h5py.File("database.h5",'r')
linear_fit_result = h5py.File("trainingresult_with_shuffle3.h5")

# 0|slope, 1|intercept, 2|r_value, 3|p_value, 4|std_err
bsl_fit_with_shuffle = np.zeros( (5, 1440, 1024, 5), dtype=np.float32)
gain_fit_with_shuffle = np.zeros( (5, 1440, 1024, 5), dtype=np.float32)
trioff_fit_with_shuffle = np.zeros( (5, 1440, 300, 5), dtype=np.float32)
test_id = np.zeros((5, 540), dtype=np.int32)

###cross-validation 
#resampling: 5-folds, shuffled 
run_id = np.arange(2700)
np.random.shuffle(run_id)
resample_id = np.zeros((5,2160), dtype=np.float32)

for training_id in range(5):
    test_region = np.arange(training_id*540,(training_id+1)*540) #(0-540),(540-1080)(1080-1620),(1620-2160),(2160-2700)
    resample_id[training_id] = np.sort(np.delete(run_id,test_region)) #(1,2160)
    test_id[training_id] = run_id[test_region] #(540)shuffled id

    DRS4_chip_temperature = my_h5_file.get("DRS4_chip_temperature")[:, :2700]
    DRS4_chip_temperature_160 = DRS4_chip_temperature[np.arange(0,1440,9),:] #(1440, 2700) to (160, 2700)

    Tmask = DRS4_chip_temperature_160[...] > 5 #(160,2700)
    Tmask[:, test_id[training_id]] = False #(160,2700)

    read_chunk_size = 9

    for pixel_chunk_id in range(1440/read_chunk_size):
        mask = Tmask[pixel_chunk_id] #(2700)
        DRS4_chip_temperature_160_with_mask = DRS4_chip_temperature_160[pixel_chunk_id, mask] #(9,2700)
    
        aBaselineMean = my_h5_file.get("BaselineMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size, :, :2700] #(9,1024,2700)
        baseline_for_chunk_with_mask = aBaselineMean[:, :, mask]  #(9,1024,2700) # ~ 240ms + ~1.2s
    
        aGainMean = my_h5_file.get("GainMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size, :, :2700] #(9,1024,2700)
        gain_for_chunk_with_mask = aGainMean[:, :, mask] #(9,1024,2700) # ~ 240ms + ~1.2s

        aTriggerOffsetMean = my_h5_file.get("TriggerOffsetMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size, :, :2700] #(9,300,2700)
        triggeroffset_for_chunk_with_mask = aTriggerOffsetMean[:, :, mask] #(9,300,2700) # ~ 240ms + ~1.2s
        
        for pixel_in_chunk_id in range(read_chunk_size):
            pixel_id = pixel_chunk_id * read_chunk_size + pixel_in_chunk_id
            print time.asctime(), "I am working on training_id: ", training_id, "pixel:", pixel_id

            for j in range(0,1024):
                #fitting 5000 points takes about 250us
                bsl_fit_with_shuffle[training_id, pixel_id, j, :] = stats.linregress(DRS4_chip_temperature_160_with_mask[...], baseline_for_chunk_with_mask[pixel_in_chunk_id, j, :])
                gain_fit_with_shuffle[training_id, pixel_id, j, :] = stats.linregress(DRS4_chip_temperature_160_with_mask[...], gain_for_chunk_with_mask[pixel_in_chunk_id, j, :])
            
            for k in range(0,300):
                trioff_fit_with_shuffle[training_id, pixel_id, k, :] = stats.linregress(DRS4_chip_temperature_160_with_mask[...], triggeroffset_for_chunk_with_mask[pixel_in_chunk_id, k, :])

test_id = linear_fit_result.create_dataset(
    name="test_id",
    data=test_id) #(5, 540)

bsl_temp_with_shuffle = linear_fit_result.create_dataset(
    name="bsl_temp_with_shuffle",
    data=bsl_fit_with_shuffle) #(5, 1440, 1024, 5)

gain_temp_with_shuffle = linear_fit_result.create_dataset(
    name="gain_temp_with_shuffle",
    data=gain_fit_with_shuffle) #(5, 1440, 1024, 5)

trioff_temp_with_shuffle = linear_fit_result.create_dataset(
    name="triggeroffset_temp_with_shuffle",
    data=trioff_fit_with_shuffle) #(5, 1440, 300, 5)

linear_fit_result.close()

my_h5_file.close()
