""" Can never have worked, undefined variables!
"""

import h5py
from scipy import stats

my_h5_file = h5py.File("database.h5")

# 0|slope, 1|intercept, 2|r_value, 3|p_value, 4|std_err
BaselineMean_temp = my_h5_file.create_dataset(
    name="BaselineMean_temp",
    shape=(1440, 1024, 5),
    dtype='float32')
BaselineRms_temp = my_h5_file.create_dataset(
    name="BaselineRms_temp",
    shape=(1440, 1024, 5),
    dtype='float32')
GainMean_temp = my_h5_file.create_dataset(
    name="GainMean_temp",
    shape=(1440, 1024, 5),
    dtype='float32')
GainRms_temp = my_h5_file.create_dataset(
    name="GainRms_temp",
    shape=(1440, 1024, 5),
    dtype='float32')
TriggerOffsetMean_temp = my_h5_file.create_dataset(
    name="TriggerOffsetMean_temp",
    shape=(1440, 300, 5),
    dtype='float32')
TriggerOffsetRms_temp = my_h5_file.create_dataset(
    name="TriggerOffsetRms_temp",
    shape=(1440, 300, 5),
    dtype='float32')

for i in range(0, 1440):
    aDRS4_chip_temperature = DRS4_chip_temperature[i]
    for j in range(0, 1024):
        aBaselineMean = BaselineMean[i, j]
        aBaselineRms = BaselineRms[i, j]
        aGainMean = GainMean[i, j]
        aGainRms = GainRms[i, j]
        BaselineMean_temp[i, j, :] = stats.linregress(aDRS4_chip_temperature, aBaselineMean)
        BaselineRms_temp[i, j, :] = stats.linregress(aDRS4_chip_temperature, aBaselineRms)
        GainMean_temp[i, j, :] = stats.linregress(aDRS4_chip_temperature, aGainMean)
        GainRms_temp[i, j, :] = stats.linregress(aDRS4_chip_temperature, aGainRms)
    for k in range(0, 300):
        aTriggerOffsetMean = TriggerOffsetMean[i, k]
        aTriggerOffsetRms = TriggerOffsetRms[i, k]
        TriggerOffsetMean_temp[i, k, :] = stats.linregress(aDRS4_chip_temperature, aTriggerOffsetMean)
        TriggerOffsetRms_temp[i, k, :] = stats.linregress(aDRS4_chip_temperature, aTriggerOffsetRms)

my_h5_file.close()
