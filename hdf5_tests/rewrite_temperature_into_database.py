import h5py
import numpy as np
import glob
from astropy.io import fits
from fact.time import fjd, iso2dt
from fact.slowdata import connect
import time

my_h5_file = h5py.File("database.h5")

if "DRS4_chip_temperature" in my_h5_file.keys():
    DRS4_chip_temperature = my_h5_file.get("DRS4_chip_temperature")
else:
    DRS4_chip_temperature = my_h5_file.create_dataset(
        name="DRS4_chip_temperature",
        shape=(1440, 3197),
        maxshape=(1440, None),
        dtype='float32')


# Exceptions
list_of_exception = []

db = connect()

for file_id, filename in enumerate(sorted(glob.glob("/scratch_loc/fact_drs.fits/*"))):
    print time.asctime(), "I am working on file:", filename

    fits_file = fits.open(filename)

    # Write in Baseline/Gain/TriggerOffset time in dataset
    start_time = fjd(iso2dt(fits_file[1].header["RUN0-BEG"]))
    stop_time = fjd(iso2dt(fits_file[1].header["RUN2-END"]))
    
    try:
        Temperature = db.FAD_CONTROL_TEMPERATURE.from_until(start_time, stop_time)
        mean_temperature = Temperature["temp"].mean(axis=0) # shape = (160,)
        DRS4_chip_temperature[:, file_id] = mean_temperature.repeat(9)
    except IndexError:
        print 'file ID: ', file_id, '/file name:', filename
        list_of_exception.append(file_id)

    fits_file.close()

if "Exception_File_ID" in my_h5_file.keys():
    Exception_File_ID = my_h5_file.get("Exception_File_ID")
else:
    Exception_File_ID = my_h5_file.create_dataset(
    name="Exception_File_ID",
    data=list_of_exception)
     
my_h5_file.close()
