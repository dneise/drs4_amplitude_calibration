import h5py
import numpy as np
from scipy import stats
import time

my_h5_file = h5py.File("database.h5",'r+')


# 0|slope, 1|intercept, 2|r_value, 3|p_value, 4|std_err
BaselineMean_temp = my_h5_file.require_dataset(
    name="BaselineMean_temp",
    shape=(1440, 1024, 5),
    maxshape=(1440, 1024, None),
    dtype='float32')

GainMean_temp = my_h5_file.require_dataset(
    name="GainMean_temp",
    shape=(1440, 1024, 5),
    maxshape=(1440, 1024, None),
    dtype='float32')

# TriggerOffsetMean_temp = my_h5_file.require_dataset(
#     name="TriggerOffsetMean_temp",
#     shape=(1440, 300, 5),
#     maxshape=(1440, 300, None),
#     dtype='float32',)


aBaselineMean = my_h5_file.get("BaselineMean")
aGainMean = my_h5_file.get("GainMean")
aTriggerOffsetMean = my_h5_file.get("TriggerOffsetMean")
DRS4_chip_temperature = my_h5_file.get("DRS4_chip_temperature")

for i in range(0,1440):
    print time.asctime(), "I am working on pixel:", i
    Tmask = DRS4_chip_temperature[i, :] > 5
    DRS4_chip_temperature_with_mask = DRS4_chip_temperature[i, :][Tmask]
    for j in range(0,1024):
        BaselineMean_temp[i, j, :] = stats.linregress(DRS4_chip_temperature_with_mask, aBaselineMean[i, j, :][Tmask])
        GainMean_temp[i, j, :] = stats.linregress(DRS4_chip_temperature_with_mask, aGainMean[i, j, :][Tmask])
    # for k in range(0,300):
    #     TriggerOffsetMean_temp[i, k, :] = stats.linregress(DRS4_chip_temperature[i, :][Tmask], aTriggerOffsetMean[i, k, :][Tmask])
    
my_h5_file.close()
