import numpy as np 
import h5py

linear_model = h5py.File("trainingresult_with_shuffle3.h5")

if "bsl_temp_averaged" in linear_model.keys():
    bsl_temp_averaged = linear_model.get("bsl_temp_averaged")
else:
    bsl_temp_averaged = linear_model.create_dataset(
        name="bsl_temp_averaged",
        shape=(1440, 1024, 5), #(pixel_id, cell_id, 5) # 0|slope, 1|intercept, 2|r_value, 3|p_value, 4|std_err
        chunks=(18, 16, 5),
        dtype='float32')

if "gain_temp_averaged" in linear_model.keys():
    gain_temp_averaged = linear_model.get("gain_temp_averaged")
else:
    gain_temp_averaged = linear_model.create_dataset(
        name="gain_temp_averaged",
        shape=(1440, 1024, 5), #(pixel_id, cell_id, 5) # 0|slope, 1|intercept, 2|r_value, 3|p_value, 4|std_err
        chunks=(18, 16, 5),
        dtype='float32')

bsl_temp = linear_model["bsl_temp_with_shuffle"] #(5, 1440, 1024, 5)
gain_temp = linear_model["gain_temp_with_shuffle"] #(5, 1440, 1024, 5)

bsl_temp_averaged[...] = np.mean(bsl_temp,axis=0)
gain_temp_averaged[...] = np.mean(gain_temp,axis=0)

linear_model.close()