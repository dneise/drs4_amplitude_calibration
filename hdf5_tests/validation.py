import h5py
import numpy as np
import time
import matplotlib.pyplot as plt

training = h5py.File("trainingresult_no_shuffle.h5")
database = h5py.File("database.h5",'r')

bsl_score = np.zeros( (5, 1440, 1024, 540), dtype=np.float32)
gain_score = np.zeros( (5, 1440, 1024, 540), dtype=np.float32)
trioff_score = np.zeros( (5, 1440, 300, 540), dtype=np.float32)

bsl_slope = training.get("bsl_temp")[:, :, :, 0] #(5, 1440, 1024, 1)
gain_slope = training.get("gain_temp")[:, :, :, 0] #(5, 1440, 1024, 1)
trioff_slope = training.get("triggeroffset_temp")[:, :, :, 0] #(5, 1440, 1024, 1)

bsl_intercept = training.get("bsl_temp")[:, :, :, 1] #(5, 1440, 1024, 1)
gain_intercept = training.get("gain_temp")[:, :, :, 1] #(5, 1440, 1024, 1)
trioff_intercept = training.get("triggeroffset_temp")[:, :, :, 1] #(5, 1440, 1024, 1)

DRS4_chip_temperature = database.get("DRS4_chip_temperature")[:,:2700] #(1440, 2700)
DRS4_chip_temperature_160 = DRS4_chip_temperature[np.arange(0,1440,9),:]# (1440, 2700) to (160, 2700)
temp_folds = np.hsplit(DRS4_chip_temperature_160, 5) #array(5,160,540)

read_chunk_size = 9

for training_id in range(5):
    
    temp_test = temp_train.pop(training_id) #(160,540)

    for pixel_chunk_id in range(1440/read_chunk_size):
    
    	bsl_measured = database.get("BaselineMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size,:,:2700] #(9, 1024, 2700)
		bsl_folds = np.dsplit(bsl_measured, 5) #(5, 9, 1024, 540)
        bsl_test = bsl_folds.pop(training_id) #(9, 1024, 540)

		gain_measured = database.get("GainMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size,:,:2700] #(9, 1024, 2700)
		gain_folds = np.dsplit(gain_measured, 5) #(5, 9, 1024, 540)
        gain_test = gain_folds.pop(training_id) #(9, 1024, 540)

        trioff_measured = database.get("TriggerOffsetMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size,:,:2700] #(9, 300, 2700)
		trioff_folds = np.dsplit(trioff_measured, 5) #(5, 9, 300, 540)
        trioff_test = trioff_folds.pop(training_id) #(9, 300, 540)

		for pixel_in_chunk_id in range(read_chunk_size):
			
			pixel_id = pixel_chunk_id * read_chunk_size + pixel_in_chunk_id
			print time.asctime(), "I am working on training_id: ", training_id, " pixel:", pixel_id

			for i in range(1024):
				
				bsl_predicted = bsl_intercept[training_id, pixel_id, i] + bsl_slope[training_id, pixel_id, i] * temp_test[pixel_chunk_id, :]#(540)
				bsl_score[training_id, pixel_id, i, :] = abs(bsl_predicted - bsl_test[pixel_in_chunk_id, i, :]) #(540)

				gain_predicted = gain_intercept[training_id, pixel_id, i] + gain_slope[training_id, pixel_id, i] * temp_test[pixel_chunk_id, :]#(540)
				gain_score[training_id, pixel_id, i, :] = abs(gain_predicted - gain_test[pixel_in_chunk_id, i, :]) / gain_test[pixel_in_chunk_id, i, :] #(540)

			for j in range(300):

				trioff_predicted = trioff_intercept[training_id, pixel_id, j] + trioff_slope[training_id, pixel_id, j] * temp_test[pixel_chunk_id, :]#(540)
				trioff_score[training_id, pixel_id, j, :] = abs(trioff_predicted - trioff_test[pixel_in_chunk_id, j, :]) #(540)

bsl_score_no_shuffle = training.create_dataset(
	name="bsl_score_no_shuffle",
	data=bsl_score)

gain_score_no_shuffle = training.create_dataset(
	name="gain_score_no_shuffle",
	data=gain_score)

trioff_score_no_shuffle = training.create_dataset(
	name="trioff_score_no_shuffle",
	data=trioff_score)

#print time.asctime(), "I started to plot histogram."

#bsl_score_1D = bsl_score.reshape(5*1440*1024*540)
#plt.hist(bsl_score_1D, bins=1000)
#gain_score_1D = gain_score.reshape(5*1440*1024*540)
#trioff_score_1D = trioff_score.reshape(5*1440*300*540)

training.close()
database.close()			