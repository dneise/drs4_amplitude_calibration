import domfits
import numpy as np 
import glob
import h5py
import time
import matplotlib.pyplot as plt

linear_model = "hdf5_tests/trainingresult_with_shuffle3.h5"
overview = h5py.File("comparison_data/overview.h5",'r')

temp_160 = overview.get("temperature") #(243,160) #ATTENTION: they are not sorted by filename

night_id = overview.get("night_id") #(243)
run_id = overview.get("run_id") #(243)

#linear_monster = np.zeros( (1440,1024), dtype=np.float32)
#measured_monster = np.zeros( (1440,1024), dtype=np.float32)
linear_monster = np.zeros( (1440, 300), dtype=np.float32)
measured_monster = np.zeros( (1440, 300), dtype=np.float32)

#filename = "/scratch_loc/comparison_data/20140204_094.fits.fz"   
filename = "/data/fact_pedestal/2014/06/30/20140630_008.fits.fz"   

#used for looking corresponding temperature
#index_array = np.where(night_id[...]==np.int32(filename[29:37])) #an array
#for i in index_array[0]:
#    if run_id[i]==np.int32(filename[38:41]):
#        index = i
#temp = temp_160[index,:]
temp = np.ones(160) * 30.

linear_file = domfits.RawDataLinearModelCalib(filename,linear_model,temp)
for row_id in range(1000):
    linear_file.next()
    linear_calib = linear_file.cols["CalibData2D"]
    linear_monster = linear_monster + linear_calib
linear_average = linear_monster/1000

#drs_filename = "/scratch_loc/comparison_data/20140204_093.drs.fits.gz"
drs_filename = "/data/fact_drs.fits/2014/06/30/20140630_007.drs.fits.gz"   

measured_file = domfits.RawData(filename, drs_filename)
for row_id in range(1000):
    measured_file.next()
    measured_calib = measured_file.cols["CalibData2D"]  # (1440,1024) 
    measured_monster = measured_monster + measured_calib
measured_average = measured_monster/1000   
  

overview.close()


plt.figure()
plt.ion()
plt.plot(measured_average[200, 20:], "b.:", label="measured")
plt.plot(linear_average[200, 20:], "r.:", label="linear")
plt.grid()
plt.legend()
plt.ylabel("calibrated amplitude [mV]")
plt.xlabel("time [0.5 ns]")
plt.title("average of 1000 pedestal events, with 2 different calibrations. Without Trigger Offset subtration.")
