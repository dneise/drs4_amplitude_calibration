import domfits
import numpy as np 
import glob
import h5py
import time
import matplotlib.pyplot as plt

linear_model = "hdf5_tests/trainingresult_with_shuffle3.h5"
overview = h5py.File("comparison_data/overview.h5",'r')

temp_160 = overview.get("temperature") #(243,160) #ATTENTION: they are not sorted by filename

night_id = overview.get("night_id") #(243)
run_id = overview.get("run_id") #(243)

linear_monster = np.zeros( (1440,1024), dtype=np.float32)
measured_monster = np.zeros( (1440,1024), dtype=np.float32)

filename = "/scratch_loc/comparison_data/20140204_094.fits.fz"   

#used for looking corresponding temperature
index_array = np.where(night_id[...]==np.int32(filename[29:37])) #an array
for i in index_array[0]:
    if run_id[i]==np.int32(filename[38:41]):
        index = i

linear_file = domfits.RawDataLinearModelCalib(filename,linear_model,temp_160[index,:])
