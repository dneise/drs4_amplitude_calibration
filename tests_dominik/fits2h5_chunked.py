import h5py
import numpy as np
import glob
from astropy.io import fits
from fact.time import fjd, iso2dt
from fact.slowdata import connect
import time

my_h5_file = h5py.File("database.h5")

files_per_chunk = 30

#Mean and RMS of Baseline, Gain, TriggerOffset
BaselineMean = my_h5_file.create_dataset(
    name="BaselineMean",
    shape=(1440, 1024, 3197),
    chunks=(18, 16, 30),
    maxshape=(1440, 1024, None),
    dtype='float32',
    compression="gzip",
    )

BaselineRms = my_h5_file.create_dataset(
    name="BaselineRms",
    shape=(1440, 1024, 3197),
    chunks=(18, 16, 30),
    maxshape=(1440, 1024, None),
    dtype='float32',
    compression="gzip",
    )

GainMean = my_h5_file.create_dataset(
    name="GainMean",
    shape=(1440, 1024, 3197),
    chunks=(18, 16, 30),
    maxshape=(1440, 1024, None),
    dtype='float32',
    compression="gzip",
    )

GainRms = my_h5_file.create_dataset(
    name="GainRms",
    shape=(1440, 1024, 3197),
    chunks=(18, 16, 30),
    maxshape=(1440, 1024, None),
    dtype='float32',
    compression="gzip",
    )

TriggerOffsetMean = my_h5_file.create_dataset(
    name="TriggerOffsetMean",
    shape=(1440, 300, 3197),
    chunks=(18, 10, 30),
    maxshape=(1440, 300, None),
    dtype='float32',
    compression="gzip",
    )

TriggerOffsetRms = my_h5_file.create_dataset(
    name="TriggerOffsetRms",
    shape=(1440, 300, 3197),
    chunks=(18, 10, 30),
    maxshape=(1440, 300, None),
    dtype='float32',
    compression="gzip",
    )

# Start and Stop time for Baseline, Gain and Trigger Offset measurement
Baseline_Starttime = my_h5_file.create_dataset(
    name="Baseline_Starttime",
    shape=(3197, ),
    maxshape=(None, ),
    dtype='float64')

Baseline_Stoptime = my_h5_file.create_dataset(
    name="Baseline_Stoptime",
    shape=(3197, ),
    maxshape=(None, ),
    dtype='float64')

Gain_Starttime = my_h5_file.create_dataset(
    name="Gain_Starttime",
    shape=(3197, ),
    maxshape=(None, ),
    dtype='float64')

Gain_Stoptime = my_h5_file.create_dataset(
    name="Gain_Stoptime",
    shape=(3197, ),
    maxshape=(None, ),
    dtype='float64')

TriggerOffset_Starttime = my_h5_file.create_dataset(
    name="TriggerOffset_Starttime",
    shape=(3197, ),
    maxshape=(None, ),
    dtype='float64')

TriggerOffset_Stoptime = my_h5_file.create_dataset(
    name="TriggerOffset_Stoptime",
    shape=(3197, ),
    maxshape=(None, ),
    dtype='float64')

#Temperature
DRS4_chip_temperature = my_h5_file.create_dataset(
    name="DRS4_chip_temperature",
    shape=(1440, 3197),
    maxshape=(1440, None),
    dtype='float32')

db = connect()

# pre allocate some memory for 30 files.
BaselineMean_chunk = np.zeros( (1440,1024,30), dtype=np.float32)
BaselineRms_chunk = np.zeros( (1440,1024,30), dtype=np.float32)
GainMean_chunk = np.zeros( (1440,1024,30), dtype=np.float32)
GainRms_chunk = np.zeros( (1440,1024,30), dtype=np.float32)
TriggerOffsetMean_chunk = np.zeros( (1440,300,30), dtype=np.float32)
TriggerOffsetRms_chunk = np.zeros( (1440,300,30), dtype=np.float32)

for file_id, filename in enumerate(sorted(glob.glob("/scratch_loc/fact_drs.fits/*"))):
    print file_id, time.asctime(), "I am working on file:", filename
    fits_file = fits.open(filename)
    chunk_id = file_id % 30

    # Write in Baseline Info in dataset
    BaselineMean_chunk[:,:,chunk_id] = fits_file[1].data["BaselineMean"].reshape(1440, 1024)
    BaselineRms_chunk[:,:,chunk_id] = fits_file[1].data["BaselineRms"].reshape(1440, 1024)
    GainMean_chunk[:,:,chunk_id] = fits_file[1].data["GainMean"].reshape(1440, 1024)
    GainRms_chunk[:,:,chunk_id] = fits_file[1].data["GainRms"].reshape(1440, 1024)
    TriggerOffsetMean_chunk[:,:,chunk_id] = fits_file[1].data["TriggerOffsetMean"].reshape(1440, 300)
    TriggerOffsetRms_chunk[:,:,chunk_id] = fits_file[1].data["TriggerOffsetRms"].reshape(1440, 300)

    if chunk_id == 29:
      chunk_number = file_id / 30
      print "    ", chunk_number, chunk_id, time.asctime(), "starting to write" 
      BaselineMean[:, :, chunk_number*30:(chunk_number+1)*30]      = BaselineMean_chunk
      BaselineRms[:, :, chunk_number*30:(chunk_number+1)*30]       = BaselineRms_chunk
      GainMean[:, :, chunk_number*30:(chunk_number+1)*30]          = GainMean_chunk
      GainRms[:, :, chunk_number*30:(chunk_number+1)*30]           = GainRms_chunk
      TriggerOffsetMean[:, :, chunk_number*30:(chunk_number+1)*30] = TriggerOffsetMean_chunk
      TriggerOffsetRms[:, :, chunk_number*30:(chunk_number+1)*30]  = TriggerOffsetRms_chunk
      print "    ", chunk_number, chunk_id, time.asctime(), "done writing." 
   
    Baseline_Starttime[file_id] = fjd(iso2dt(fits_file[1].header["RUN0-BEG"]))
    Baseline_Stoptime[file_id] = fjd(iso2dt(fits_file[1].header["RUN0-END"]))
    Gain_Starttime[file_id] = fjd(iso2dt(fits_file[1].header["RUN1-BEG"]))
    Gain_Stoptime[file_id] = fjd(iso2dt(fits_file[1].header["RUN1-END"]))
    TriggerOffset_Starttime[file_id] = fjd(iso2dt(fits_file[1].header["RUN2-BEG"]))
    TriggerOffset_Stoptime[file_id] = fjd(iso2dt(fits_file[1].header["RUN2-END"]))
    
    #Write in temp in dataset
    start_time = Baseline_Starttime[file_id]
    stop_time = TriggerOffset_Stoptime[file_id]
    try:
        Temperature = db.FAD_CONTROL_TEMPERATURE.from_until(start_time, stop_time)
        for i in range(0, 160): 
           	DRS4_chip_temperature[i:(i+9), file_id] = np.mean(Temperature["temp"][:,i])
    except IndexError:
        print "wasn't able to retrieve temperature for", filename
        for i in range(0, 160):
            DRS4_chip_temperature[i:(i+9), file_id] = np.nan

    fits_file.close()

my_h5_file.close()
