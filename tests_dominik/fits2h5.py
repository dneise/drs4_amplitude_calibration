import h5py
import numpy as np
import glob
from astropy.io import fits
from fact.time import fjd, iso2dt
from fact.slowdata import connect
import time

my_h5_file = h5py.File("database.h5")

#Mean and RMS of Baseline, Gain, TriggerOffset
BaselineMean = my_h5_file.create_dataset(
    name="BaselineMean",
    shape=(1440, 1024, 3197),
    chunks=(18, 16, 30),
    maxshape=(1440, 1024, None),
    dtype='float32',
    compression="gzip",
    )

BaselineRms = my_h5_file.create_dataset(
    name="BaselineRms",
    shape=(1440, 1024, 3197),
    chunks=(18, 16, 30),
    maxshape=(1440, 1024, None),
    dtype='float32',
    compression="gzip",
    )

GainMean = my_h5_file.create_dataset(
    name="GainMean",
    shape=(1440, 1024, 3197),
    chunks=(18, 16, 30),
    maxshape=(1440, 1024, None),
    dtype='float32',
    compression="gzip",
    )

GainRms = my_h5_file.create_dataset(
    name="GainRms",
    shape=(1440, 1024, 3197),
    chunks=(18, 16, 30),
    maxshape=(1440, 1024, None),
    dtype='float32',
    compression="gzip",
    )

TriggerOffsetMean = my_h5_file.create_dataset(
    name="TriggerOffsetMean",
    shape=(1440, 300, 3197),
    chunks=(18, 10, 30),
    maxshape=(1440, 300, None),
    dtype='float32',
    compression="gzip",
    )

TriggerOffsetRms = my_h5_file.create_dataset(
    name="TriggerOffsetRms",
    shape=(1440, 300, 3197),
    chunks=(18, 10, 30),
    maxshape=(1440, 300, None),
    dtype='float32',
    compression="gzip",
    )

# Start and Stop time for Baseline, Gain and Trigger Offset measurement
Baseline_Starttime = my_h5_file.create_dataset(
    name="Baseline_Starttime",
    shape=(3197, ),
    maxshape=(None, ),
    dtype='float64')

Baseline_Stoptime = my_h5_file.create_dataset(
    name="Baseline_Stoptime",
    shape=(3197, ),
    maxshape=(None, ),
    dtype='float64')

Gain_Starttime = my_h5_file.create_dataset(
    name="Gain_Starttime",
    shape=(3197, ),
    maxshape=(None, ),
    dtype='float64')

Gain_Stoptime = my_h5_file.create_dataset(
    name="Gain_Stoptime",
    shape=(3197, ),
    maxshape=(None, ),
    dtype='float64')

TriggerOffset_Starttime = my_h5_file.create_dataset(
    name="TriggerOffset_Starttime",
    shape=(3197, ),
    maxshape=(None, ),
    dtype='float64')

TriggerOffset_Stoptime = my_h5_file.create_dataset(
    name="TriggerOffset_Stoptime",
    shape=(3197, ),
    maxshape=(None, ),
    dtype='float64')

#Temperature
DRS4_chip_temperature = my_h5_file.create_dataset(
    name="DRS4_chip_temperature",
    shape=(1440, 3197),
    maxshape=(1440, None),
    dtype='float32')

db = connect()

for file_id, filename in enumerate(sorted(glob.glob("/scratch_loc/fact_drs.fits/*"))):
    print file_id, time.asctime(), "I am working on file:", filename

    fits_file = fits.open(filename)

    # Write in Baseline Info in dataset
    this_files_BaselineMean = fits_file[1].data["BaselineMean"].reshape(1440, 1024)
    this_files_BaselineRms = fits_file[1].data["BaselineRms"].reshape(1440, 1024)
    
    BaselineMean[:, :, file_id] = this_files_BaselineMean
    BaselineRms[:, :, file_id] = this_files_BaselineRms
   
    Baseline_Starttime[file_id] = fjd(iso2dt(fits_file[1].header["RUN0-BEG"]))
    Baseline_Stoptime[file_id] = fjd(iso2dt(fits_file[1].header["RUN0-END"]))

	# Write in Gain Info in dataset
    this_files_GainMean = fits_file[1].data["GainMean"].reshape(1440, 1024)
    this_files_GainRms = fits_file[1].data["GainRms"].reshape(1440, 1024)
    
    GainMean[:, :, file_id] = this_files_GainMean
    GainRms[:, :, file_id] = this_files_GainRms
   
    Gain_Starttime[file_id] = fjd(iso2dt(fits_file[1].header["RUN1-BEG"]))
    Gain_Stoptime[file_id] = fjd(iso2dt(fits_file[1].header["RUN1-END"]))
    fits_file.close()

    # Write in TriggerOffset Info in dataset
    this_files_TriggerOffsetMean = fits_file[1].data["TriggerOffsetMean"].reshape(1440, 300)
    this_files_TriggerOffsetRms = fits_file[1].data["TriggerOffsetRms"].reshape(1440, 300)
    
    TriggerOffsetMean[:, :, file_id] = this_files_TriggerOffsetMean
    TriggerOffsetRms[:, :, file_id] = this_files_TriggerOffsetRms
   
    TriggerOffset_Starttime[file_id] = fjd(iso2dt(fits_file[1].header["RUN2-BEG"]))
    TriggerOffset_Stoptime[file_id] = fjd(iso2dt(fits_file[1].header["RUN2-END"]))
    
    #Write in temp in dataset
    start_time = Baseline_Starttime[file_id]
    stop_time = TriggerOffset_Stoptime[file_id]
    Temperature = db.FAD_CONTROL_TEMPERATURE.from_until(start_time, stop_time)
    for i in range(0, 160): 
     	DRS4_chip_temperature[i:(i+9), file_id] = np.mean(Temperature["temp"][:,i])

    fits_file.close()

my_h5_file.close()
