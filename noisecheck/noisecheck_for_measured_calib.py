import domfits
import numpy as np 
import glob
import h5py
import time

comparison_data_overview_file = h5py.File('/scratch_loc/comparison_data/overview.h5', 'r')
output_file = h5py.File("measured_calib_noise_withcut_no_triggeroffset.h5")

test_file_paths = sorted(glob.glob("/scratch_loc/comparison_data/*.fits.fz"))
N_files = len(test_file_paths)

# "rows" or "events" are used as synonyms, when talking about Fits files.
N_rows_per_file = 1000

std_dev = output_file.require_dataset(
    name="std_dev",
    shape=(N_files, N_rows_per_file, 1440), #(files, events, pixels)
    dtype='float32')
std_dev_cut = output_file.require_dataset(
    name="std_dev_cut",
    shape=(N_files, N_rows_per_file, 1440), #(files, events, pixels)
    dtype='float32')

for file_id, filename in enumerate(test_file_paths):
    try:
        # this is a lucky coincidence, and not a rule
        # it does not have to be that easy, to derive the calib_file runnumber 
        # from the data file.
        calib_file_run_number = int(filename[38:41]) - 1
        drs_filename = (filename[:38] 
                        + "{0:03d}.drs.fits.gz".format(calib_file_run_number)
                       )
        calib_data_file = domfits.RawData(filename, drs_filename)

        for row_id, row in enumerate(calib_data_file):
            if row_id % 100 == 0:
                print time.asctime(), "file_id", file_id, "row_id", row_id
            calib_data = calib_data_file.cols["CalibData2D"]  # (1440,1024)
            std_dev[file_id, row_id, :] = calib_data.std(axis=1)
            std_dev_cut[file_id, row_id, :] = calib_data[:, 20:1014].std(axis=1)
    except:
        continue
output_file.close()
