import domfits
import numpy as np 
import glob
import h5py
import time

database = h5py.File("hdf5_tests/database.h5",'r')
output_file = h5py.File("cons_model_noise.h5")

if "std_wrt_time" in output_file.keys():
    std_wrt_time = output_file.get("std_wrt_time")
else:
    std_wrt_time = output_file.create_dataset(
        name="std_wrt_time",
        shape=(243,1000,1440), #(files, events, pixels)
        dtype='float32')

read_chunk_size = 9
cali_data = np.zeros( (1000, 1440, 1024), dtype=np.float32)

for file_id, filename in enumerate(sorted(glob.glob("/scratch_loc/comparison_data/*.fits.fz"))):
    raw_data_file = domfits.Fits(filename)
    file_length = len(raw_data_file)

    for row_id in range(file_length): #1000
        #print time.asctime(), "file_id", file_id, "row_id", row_id
        raw_data_file.next()

        raw_data = raw_data_file.cols["Data"].reshape(1440,1024)
        
        for pixel_chunk_id in range(1440/read_chunk_size):
            
            bsl_measured = database.get("BaselineMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size,:,:2700] #(9, 1024, 2700)
            gain_measured = database.get("GainMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size,:,:2700] #(9, 1024, 2700)

            for pixel_in_chunk_id in range(read_chunk_size):
                
                pixel_id = pixel_chunk_id * read_chunk_size + pixel_in_chunk_id
                print time.asctime(), "file_id", file_id, "row_id", row_id,"pixel_id",pixel_id
                
                bsl_predicted = np.mean(bsl_measured[pixel_in_chunk_id,:,:],axis=1)#[:, np.newaxis]#(1024, 2700)->(1024,1)
                gain_predicted = np.mean(gain_measured[pixel_in_chunk_id,:,:],axis=1)#[:, np.newaxis]  #(1024, 2700)->(1024,1)
                
                cali_data[row_id,pixel_id,:] = (raw_data[pixel_id,:] * 4096. / 2000. - bsl_predicted) * 1907.35 / gain_predicted #(1024) #bsl
                std_wrt_time[file_id,row_id,pixel_id] = np.std(cali_data[row_id,pixel_id,10:]) #std(243,1000,1440) #cali_data(1000,1440,1024)
        
output_file.close()