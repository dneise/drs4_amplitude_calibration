import domfits
import numpy as np 
import glob
import h5py
import time

#database = h5py.File("hdf5_tests/database.h5",'r')
linear_model = "hdf5_tests/trainingresult_with_shuffle3.h5"
overview = h5py.File("comparison_data/overview.h5",'r')
output_file = h5py.File("linear_model_noise_dom_full_list.h5")

if "std_of_cells" in output_file.keys():
    std_of_cells = output_file.get("std_of_cells")
else:
    std_of_cells = output_file.create_dataset(
        name="std_of_cells",
        shape=(243, 1000, 1440), #(files, events, pixels)
        dtype='float32')


temp_160 = overview.get("temperature") #(243,160) #ATTENTION: they are not sorted by filename

night_id = overview.get("night_id") #(243)
run_id = overview.get("run_id") #(243)

start_cells = np.zeros( (1000, 1440), dtype=np.int16)
cali_data = np.zeros( (1000, 1440, 1024), dtype=np.float32)
std = np.zeros((243,1000,1440),dtype=np.float32)

for file_id, filename in enumerate(sorted(glob.glob("/scratch_loc/comparison_data/*.fits.fz"))):
    print filename 
    raw_data_file = domfits.RawDataLinearModelCalib(
       filename,
       linear_model,
       temp_160[file_id])
    file_length = len(raw_data_file)

    #used for looking corresponding temperature
    index_array = np.where(night_id[...]==np.int32(filename[29:37])) #an array
    for i in index_array[0]:
        if run_id[i]==np.int32(filename[38:41]):
            index = i

    for row_id in range(file_length): #1000
        print time.asctime(), "file_id", file_id, "row_id", row_id
        
        raw_data_file.next()

        #calibration applied linear model
        calib_data = raw_data_file.cols["CalibData2D"]
        std_of_cells[file_id, row_id, :] = calib_data[:, 10:].std(axis=1)

#std_of_cells[...] = np.mean(std, axis=1) #(243,1440)
#std_of_cells[...] = std #(243,1000,1440)

#database.close()
overview.close()
output_file.close()
