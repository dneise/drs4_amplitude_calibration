import domfits
import numpy as np 
import glob
import h5py
import time

database = h5py.File("hdf5_tests/database.h5",'r')
linear_model = h5py.File("hdf5_tests/trainingresult_with_shuffle3.h5",'r')
overview = h5py.File("comparison_data/overview.h5",'r')
output_file = h5py.File("linear_model_noise.h5")

if "std_of_cells" in output_file.keys():
    std_of_cells = output_file.get("std_of_cells")
else:
    std_of_cells = output_file.create_dataset(
        name="std_of_cells",
        shape=(243, 1000, 1440), #(files, events, pixels)
        dtype='float32')

bsl_slope = linear_model.get("bsl_temp_averaged")[:, :, 0] #(1440, 1024, 1)
gain_slope = linear_model.get("gain_temp_averaged")[:, :, 0] #(1440, 1024, 1)

bsl_intercept = linear_model.get("bsl_temp_averaged")[:, :, 1] #(1440, 1024, 1)
gain_intercept = linear_model.get("gain_temp_averaged")[:, :, 1] #(1440, 1024, 1)

temp_160 = overview.get("temperature") #(243,160) #ATTENTION: they are not sorted by filename
temp_1024 = np.repeat(temp_160,9,axis=1) #(243,160)->(243,1024)

night_id = overview.get("night_id") #(243)
run_id = overview.get("run_id") #(243)

start_cells = np.zeros( (1000, 1440), dtype=np.int16)
cali_data = np.zeros( (1000, 1440, 1024), dtype=np.float32)
std = np.zeros((243,1000,1440),dtype=np.float32)

for file_id, filename in enumerate(sorted(glob.glob("/scratch_loc/comparison_data/*.fits.fz"))):
    raw_data_file = domfits.Fits(filename)
    file_length = len(raw_data_file)

    #used for looking corresponding temperature
    index_array = np.where(night_id[...]==np.int32(filename[29:37])) #an array
    for i in index_array[0]:
        if run_id[i]==np.int32(filename[38:41]):
            index = i

    for row_id in range(file_length): #1000
        print time.asctime(), "file_id", file_id, "row_id", row_id
        
        raw_data_file.next()

        #calibration applied linear model
        raw_data = raw_data_file.cols["Data"].reshape(1440,1024)
    
        bsl_predicted = bsl_intercept[...] + bsl_slope[...] * temp_1024[index,:][:,np.newaxis]#(1440,1024)
        gain_predicted = gain_intercept[...] + gain_slope[...] * temp_1024[index, :][:,np.newaxis] #(1440,1024)

        #now rotate the cali_data to be compatible to raw_data, since we need to cut the first 10 points of raw_data
        start_cells[row_id] = raw_data_file.cols["StartCellData"] # (1440)

        #cali_data[row_id,:,:] = (raw_data * 4096. / 2000. - bsl_predicted) * 1907.35 / gain_predicted #(1440,1024) #bsl
        
        for pixel_id in range(1440):
            bsl_predicted[pixel_id,:] = np.roll(bsl_predicted[pixel_id,:],-start_cells[row_id, pixel_id])
            gain_predicted[pixel_id,:] = np.roll(gain_predicted[pixel_id,:],-start_cells[row_id, pixel_id])
            cali_data[row_id,pixel_id,:] = (raw_data[pixel_id,:] * 4096. / 2000. - bsl_predicted[pixel_id,:]) * 1907.35 / gain_predicted[pixel_id,:] #(1024) #bsl
            #cali_data[row_id, pixel_id,:] = np.roll(cali_data[row_id, pixel_id,:], -start_cells[row_id, pixel_id]) #1024
            std_of_cells[file_id,row_id,pixel_id] = np.std(cali_data[row_id,pixel_id,10:]) #std(243,1000,1440) #cali_data(1000,1440,1024)
    
#std_of_cells[...] = np.mean(std, axis=1) #(243,1440)
#std_of_cells[...] = std #(243,1000,1440)

database.close()
linear_model.close()
overview.close()
output_file.close()