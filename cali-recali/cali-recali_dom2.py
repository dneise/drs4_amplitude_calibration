import h5py
import numpy as np
import time

training = h5py.File("hdf5_tests/trainingresult_with_shuffle3.h5", "r")
database = h5py.File("hdf5_tests/database.h5",'r')

output_file = h5py.File("name_lala.h5")


if "deviation" in output_file.keys():
    deviation = output_file.get("deviation")
else:
    deviation = output_file.create_dataset(
        name="deviation",
        shape=(3, 5, 1440, 1024, 540), #(u,training_id, pixel_id, cell_id, test_size)
        chunks=(3, 5, 18, 16, 20),
        dtype='float32')

bsl_slope = training.get("bsl_temp_with_shuffle")[:, :, :, 0] #(5, 1440, 1024, 1)
gain_slope = training.get("gain_temp_with_shuffle")[:, :, :, 0] #(5, 1440, 1024, 1)

bsl_intercept = training.get("bsl_temp_with_shuffle")[:, :, :, 1] #(5, 1440, 1024, 1)
gain_intercept = training.get("gain_temp_with_shuffle")[:, :, :, 1] #(5, 1440, 1024, 1)

test_id = training.get("test_id")[...] #(5,540)

DRS4_chip_temperature = database.get("DRS4_chip_temperature")[:,:2700] #(1440, 2700)
#DRS4_chip_temperature_160 = DRS4_chip_temperature[np.arange(0,1440,9),:]# (1440, 2700) to (160, 2700)

read_chunk_size = 4*36

for l, u_initial in enumerate([10, 100, 1000]):

  for training_id in range(5):
     
    temp_test = DRS4_chip_temperature[:,test_id[training_id, :]] #(1440,540)

    for pixel_chunk_id in range(1440/read_chunk_size):
      bsl_measured = database.get("BaselineMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size,:,:2700] 
      bsl_test = bsl_measured[:, :, test_id[training_id, :]] #(9, 1024, 540)
     
      gain_measured = database.get("GainMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size,:,:2700]
      gain_test = gain_measured[:, :, test_id[training_id, :]] #(9, 1024, 540)

      pix_beg, pix_end = pixel_chunk_id * read_chunk_size, pixel_chunk_id+1 * read_chunk_size

      # bsl_predicted = (144, 1024, new) + (144, 1024, new) * (144, new, 540) = (144, 1024, 540) --> 300 MB
      bsl_predicted = (bsl_intercept[training_id, pix_beg:pix_end, :, np.newaxis] 
                        + bsl_slope[training_id, pix_beg:pix_end, :, np.newaxis] 
                        * temp_test[pix_beg:pix_end, np.newaxis, :])
      gain_predicted = (gain_intercept[training_id, pix_beg:pix_end, :, np.newaxis]
                         + gain_slope[training_id, pix_beg:pix_end, :, np.newaxis]
                         * temp_test[pix_beg:pix_end, np.newaxis, :])
      a = (u_initial * gain_test /1907.35 + bsl_test) * 2000. / 4096. #ADC voltage #(144, 1024, 540) --> 300MB
      
      u = (a * 4096. / 2000. - bsl_predicted) * 1907.35 / gain_predicted # --> 300MB
      deviation[l, training_id, pix_beg:pix_end, :, :] = u / u_initial -1 #(1024, 540)
      print time.asctime(), " u_initial = ", u_initial, " training_id: ", training_id, " pixel:", pix_beg, pix_end

training.close()
database.close()
output_file.close()
