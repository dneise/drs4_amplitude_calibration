import h5py
import numpy as np
import time

database = h5py.File("hdf5_tests/database.h5",'r')

output_file = h5py.File("deviation_cons_measured.h5")


if "deviation_cons" in output_file.keys():
    deviation_cons = output_file.get("deviation_cons")
else:
    deviation_cons = output_file.create_dataset(
        name="deviation_cons",
        shape=(3, 1440, 1024, 2700), #(u,training_id, pixel_id, cell_id, test_size)
        chunks=(1, 18, 16, 27),
        dtype='float32')


read_chunk_size = 4*36

for l in range(3):
     
  u_initial = 10**(l+1)

  for pixel_chunk_id in range(1440/read_chunk_size):
  
    bsl_measured = database.get("BaselineMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size,:,:2700] #(9, 1024, 2700)

    gain_measured = database.get("GainMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size,:,:2700] #(9, 1024, 2700)

    for pixel_in_chunk_id in range(read_chunk_size):
          
      pixel_id = pixel_chunk_id * read_chunk_size + pixel_in_chunk_id
      print time.asctime(), " u_initial = ", u_initial, " pixel:", pixel_id

      bsl_predicted = np.mean(bsl_measured[pixel_in_chunk_id,:,:],axis=1)[:, np.newaxis]#(1024, 2700)->(1024,1)
      gain_predicted = np.mean(gain_measured[pixel_in_chunk_id,:,:],axis=1)[:, np.newaxis]  #(1024, 2700)->(1024,1)
        
      a = (u_initial * gain_measured[pixel_in_chunk_id,:,:] /1907.35 + bsl_measured[pixel_in_chunk_id,:,:]) * 2000. / 4096. #ADC voltage #(1024, 2700)
      u = (a * 4096. / 2000. - bsl_predicted) * 1907.35 / gain_predicted #(1024, 2700)
      
      deviation_cons[l, pixel_id, :, :] = u - u_initial #(1024, 2700)

database.close()
output_file.close()
