import h5py
import numpy as np
import time

training = h5py.File("hdf5_tests/trainingresult_with_shuffle3.h5", "r")
database = h5py.File("hdf5_tests/database.h5",'r')

output_file = h5py.File("deviation_linear_measured_dom.h5")


if "deviation" in output_file.keys():
    deviation = output_file.get("deviation")
else:
    deviation = output_file.create_dataset(
        name="deviation",
        shape=(3, 1440, 1024, 2700), #(u, pixel_id, cell_id)
        chunks=(3, 18, 16, 27),
        dtype='float32')

bsl_slope = training.get("bsl_temp_averaged")[:, :, 0] #(1440, 1024, 1)
gain_slope = training.get("gain_temp_averaged")[:, :, 0] #(1440, 1024, 1)

bsl_intercept = training.get("bsl_temp_averaged")[:, :, 1] #(1440, 1024, 1)
gain_intercept = training.get("gain_temp_averaged")[:, :, 1] #(1440, 1024, 1)

##test_id = training.get("test_id") #(5,540)

DRS4_chip_temperature = database.get("DRS4_chip_temperature")[:,:2700] #(1440, 2700)
#DRS4_chip_temperature_160 = DRS4_chip_temperature[np.arange(0,1440,9),:]# (1440, 2700) to (160, 2700)

read_chunk_size = 4*36

for l in range(3):
     
  u_initial = 10**(l+1)

  #for training_id in range(5):
     
  #temp_test = DRS4_chip_temperature[:,test_id[training_id, :]] #(1440,540)

  for pixel_chunk_id in range(1440/read_chunk_size):
    
    bsl_measured = database.get("BaselineMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size,:,:2700] #(4*36, 1024, 2700)
#   bsl_test = bsl_measured[:, :, test_id[training_id, :]] #(9, 1024, 540)

    gain_measured = database.get("GainMean")[pixel_chunk_id*read_chunk_size:(pixel_chunk_id+1)*read_chunk_size,:,:2700] #(4*36, 1024, 2700)
#   gain_test = gain_measured[:, :, test_id[training_id, :]] #(9, 1024, 540)

    for pixel_in_chunk_id in range(read_chunk_size):
            
      pixel_id = pixel_chunk_id * read_chunk_size + pixel_in_chunk_id
      print time.asctime(), " u_initial = ", u_initial, " pixel:", pixel_id

      bsl_predicted = bsl_intercept[pixel_id,:][:, np.newaxis] + bsl_slope[pixel_id,:][:, np.newaxis] * DRS4_chip_temperature[pixel_id, :][np.newaxis, :] #(1024, 2700)
      gain_predicted = gain_intercept[pixel_id,:][:, np.newaxis] + gain_slope[pixel_id,:][:, np.newaxis] * DRS4_chip_temperature[pixel_id, :][np.newaxis, :] #(1024, 2700)
          
      a = (u_initial * gain_measured[pixel_in_chunk_id,:,:] /1907.35 + bsl_measured[pixel_in_chunk_id,:,:]) * 2000. / 4096. #ADC voltage #(1024, 2700)
      u = (a * 4096. / 2000. - bsl_predicted) * 1907.35 / gain_predicted #(1024, 2700)
        
      deviation[l, pixel_id, :, :] = u - u_initial #(1024, 2700)

training.close()
database.close()
output_file.close()
