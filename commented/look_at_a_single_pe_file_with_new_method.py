""" I THINK:
this script looks at a single_pe data file, calibrating it with yifans new method
in order to find iout if the overall scale looks okay.

"""
import domfits
import numpy as np 
import glob
import h5py
import time

linear_model = h5py.File("hdf5_tests/trainingresult_with_shuffle3.h5",'r')
output_file = h5py.File("single_pe__delete_me.h5")

calib_sig = output_file.require_dataset(
    name="calib_sig",
    shape=(10000, 1440,1024), #( events, pixels, cells)
    dtype='float32')

temp = np.loadtxt('20150430_009_temperature.txt', dtype= 'float32')
temp_1440 = np.repeat(temp,9) #(160)->(1440)

bsl_slope = linear_model.get("bsl_temp_averaged")[:, :, 0] #(1440, 1024, 1)
gain_slope = linear_model.get("gain_temp_averaged")[:, :, 0] #(1440, 1024, 1)

bsl_intercept = linear_model.get("bsl_temp_averaged")[:, :, 1] #(1440, 1024, 1)
gain_intercept = linear_model.get("gain_temp_averaged")[:, :, 1] #(1440, 1024, 1)

start_cells = np.zeros( (1000, 1440), dtype=np.int16)
cali_data = np.zeros( (1000, 1440, 1024), dtype=np.float32)
#std = np.zeros((243,1000,1440),dtype=np.float32)

for file_id, filename in enumerate(sorted(glob.glob("/data/fact_1pe/2015/04/30/20150430_009.fits.fz"))):
    raw_data_file = domfits.Fits(filename)
    file_length = len(raw_data_file)

    for row_id in range(file_length): #1000
        print time.asctime(), "file_id", file_id, "row_id", row_id
        
        raw_data_file.next()

        #calibration applied linear model
        raw_data = raw_data_file.cols["Data"].reshape(1440,1024)
    
        bsl_predicted = bsl_intercept[...] + bsl_slope[...] * temp_1440[...][:,np.newaxis]#(1440,1024)
        gain_predicted = gain_intercept[...] + gain_slope[...] * temp_1440[...][:,np.newaxis] #(1440,1024)

        #now rotate the cali_data to be compatible to raw_data, since we need to cut the first 10 points of raw_data
        start_cells[row_id] = raw_data_file.cols["StartCellData"] # (1440)

        #cali_data[row_id,:,:] = (raw_data * 4096. / 2000. - bsl_predicted) * 1907.35 / gain_predicted #(1440,1024) #bsl
        
        for pixel_id in range(1440):
            bsl_predicted[pixel_id,:] = np.roll(bsl_predicted[pixel_id,:],-start_cells[row_id, pixel_id])
            gain_predicted[pixel_id,:] = np.roll(gain_predicted[pixel_id,:],-start_cells[row_id, pixel_id])
            cali_data[row_id,pixel_id,:] = (raw_data[pixel_id,:] * 2000. / 4096.  - bsl_predicted[pixel_id,:]) * 1907.35 / gain_predicted[pixel_id,:] #(1024) #bsl
            #cali_data[row_id, pixel_id,:] = np.roll(cali_data[row_id, pixel_id,:], -start_cells[row_id, pixel_id]) #1024
        break
    break

calib_sig[...] = cali_data  

linear_model.close()
output_file.close()