""" I THINK:
this script tries to explain Yifan how the amplitude calibration works,
but only the Baseline part of it. 
It has no other use.
"""
#import root_numpy
import numpy as np
import matplotlib.pyplot as plt
plt.ion()
import time

import domfits # contains classes, that help reading special fits.fz files.

# have a look here, so study what a FACT rawfile might contain.
# http://fact-project.org/run_db/db/fact_runinfo.php?fRunTypeName=On&fRunTypeKEY=0&fSourceName=On&fSourceKEY=0&fRunStart=On&TimeDiff%28fRunStop%2CfRunStart%29=On&fROI=On&Round%28fNumPhysicsTrigger%2FTime_to_sec%28TimeDiff%28fRunStop%2CfRunStart%29%29%2C1%29=On&fNumPhysicsTrigger=On&fNumPedestalTrigger=On&fNumELPTrigger=On&fThresholdMedian=On&fZenithDistanceMean=On&fAzimuthMean=On&fStartDate=20150109&fStopDate=20150109&fNumResults=200&fSortBy=Run+
fits_file = domfits.Fits("/data/dneise/20150109_001.fits.fz")

# region of interest can be either 1024 or 300 in FACT ... 
region_of_interest = fits_file.header["NROI"]

# Normally FACT raw files are way to large, to read them fully into memory
# an work with them, so one need to use them, event-by-event.
# But in this special case, I know they only contain 1000 events, 
# which makes them roughly 3GB large

# So first I allocate a large piece of memory for the raw data:
# raw data is stored as 16bit integers (really raw ADC output)
raw_data = np.zeros( (1000, 1440, region_of_interest), dtype=np.int16)
start_cells = np.zeros( (1000, 1440), dtype=np.int16)

# Now I read out the events from the fits_file. This is different from the 
# way we are used to work with fits files, I am sorry for that ...

# A fits file is organized like a table, so there are rows, and column
file_length = len(fits_file)

# Here we iterate over all rows ... rows are also called 'events'
start_time = time.time()
for row_id in range(file_length):
	# we read out the next event ... or next 'row'
	fits_file.next() 
	
	# by calling the fits_file.next() method, the fits_file object 
	# was changed. It now *contains* the data of all columns of 
	# the current row.
	# The columns have names, and we can get the content of a column
	# by using its name. 
	# One can find out more about the names, shapes and types of columns like this:
	#    for name in fits_file.cols:
	#        print name, fits_file.cols[name].shape, fits_file.cols[name].dtype

	start_cells[row_id] = fits_file.cols["StartCellData"]
	# We stored here the contents of the column called 'StartCellData' 
	# inside one row of an array called start_cells 
	rawdata_2d = fits_file.cols["Data"].reshape(1440, region_of_interest)
	for pixel_id in range(1440):
		raw_data[row_id, pixel_id] = np.roll( rawdata_2d[pixel_id], start_cells[row_id, pixel_id] )
		
print "total time for reading a file with 1000 events:", time.time() - start_time, "seconds"

#h = ROOT.TH1D("h", "title", 101, -1900, -1800)
#root_numpy.fill_hist(h, raw_data[:,0,0])
#h.Draw()
