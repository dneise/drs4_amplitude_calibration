""" I THINK:
this script creates the big histogram root plot in yifans report, which
shows "\Delta u" for 3 values of u_0. 
"""
import numpy as np 
import root_numpy 
import h5py
import ROOT
import time

data = h5py.File("deviation_linear_measured.h5",'r')
dev = data["deviation"] #(3,1440,1024,2700)
#dev_cons = data["deviation_cons"] #(3, 1440, 1024, 2700)

hists = []
FWHM = []
Overflow = []
Underflow = []

for i in range(3):
    h = ROOT.TH1D("h"+str(i+1), "deviation of u", 1000, -40, 30)
    hists.append(h)


for j in range(10):
    print time.asctime(), "reading part:", j
    dev_part = dev[:,144*j:144*(j+1), :, :] #(3,144,1024,2700)
    dev_part_1D = dev_part.reshape((3,-1))
    #dev_cons_part = dev_cons[:,144*j:144*(j+1), :, :] #(3, 144, 1024, 2700)
    #dev_cons_part_1D = dev_cons_part.reshape((3,-1))
    for i in range(3):
        root_numpy.fill_hist(hists[i] , dev_part_1D[i])
    #for k in range(3,6):
    #    root_numpy.fill_hist(hists[i] , dev_cons_part_1D[i])

for i, h in enumerate(hists):
    bin1 = h.FindFirstBinAbove(h.GetMaximum()/2)
    bin2 = h.FindLastBinAbove(h.GetMaximum()/2)
    fwhm = h.GetBinCenter(bin1) - h.GetBinCenter(bin2)
    overflow = h.GetBinContent(1001)
    underflow = h.GetBinContent(0)
    FWHM.append(fwhm)
    Overflow.append(overflow)
    Underflow.append(underflow)
    print "u_initial = "+str(10**(i+1)), ", overflow =" ,overflow,", underflow = ", underflow

canvas = ROOT.TCanvas("canvas","deviation of u",800,600)
canvas.SetLogy()

#for i,h in enumerate(hists):
for i in range(3):
    hists[i].SetLineColor(i+1)
    if i == 0:
        hists[i].Draw("")
    else:
        hists[i].Draw("SAMES")


# These two calls to Modified() and Update() are 
# needed to inform the plotting backend, that the plot should really be made.
# Somehow the TPaveStats object are only created, 
# when the plots are really plotted.
# So when the Modified() Update() calls are missing, 
# FindObject("stats") does return nothing valid 
canvas.Modified()
canvas.Update()


stats_boxes = [h.FindObject("stats") for h in hists]

for j,stats_box in enumerate(stats_boxes):
    stats_box.SetX1NDC(0.1)
    stats_box.SetX2NDC(0.25)
    stats_box.SetY1NDC(0.8-0.1*j)
    stats_box.SetY2NDC(0.9-0.1*j)
    stats_box.Draw("same")


legend=ROOT.TLegend(0.7,0.55,0.9,0.9)
for i, h in enumerate(hists):
    legend.AddEntry(h, "u_initial = "+str(10**(i+1)) +", FWHM = "+str(FWHM[i]), "l")
    legend.AddEntry(h, "overflow ="+str(Overflow[i])+", underflow = "+str(Underflow[i]))
legend.Draw()

canvas.Modified()
canvas.Update()

canvas.SaveAs('u_deviation_linear_model.png')
canvas.SaveAs('u_deviation_linear_model.root')

data.close()
