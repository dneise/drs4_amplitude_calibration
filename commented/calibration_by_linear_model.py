""" This litte script performs a DRS4 amplitude calibration on a
drs-pedestal comparison file. So one can observe how it performs.

It shows the average of 1000 events for a certain pixel_id.

input:
 * filename of the comparison drs-pedestal file
 * overview.h5 in order to know the DRS4 temperature
 * trainingresult_with_shuffle3.h5 in order to know the baseline and gain calibration model.
"""
import domfits
import numpy as np 
import h5py
import time
import matplotlib.pyplot as plt
plt.ion()


filename = "/scratch_loc/comparison_data/20140204_094.fits.fz"
pixel_id = 100   
linear_model = "hdf5_tests/trainingresult_with_shuffle3.h5"
overview = h5py.File("comparison_data/overview.h5",'r')

temp_160 = overview.get("temperature")[...] #(243,160) #ATTENTION: they are not sorted by filename
night_ints = overview.get("night_id")[...] #(243)
run_ids = overview.get("run_id")[...] #(243)
overview.close()

def night_int_run_id_from_path(path):
	filename = path.split('/')[-1]
	without_extension = filename.split('.')[0]
	night_int = int(without_extension[:8])
	run_id = int(without_extension[-3:])
	return night_int, run_id

night_int, run_id = night_int_run_id_from_path(filename)
temperature = temp_160[(night_ints==night_int) & (run_ids==run_id)]
calib_data_file = domfits.RawDataLinearModelCalib(
	filename,
	linear_model,
	temperature,
	)

calib_data_onepixel=np.zeros(
	(len(calib_data_file), 970),
	dtype='float32'
	)

for row_id, row in enumerate(calib_data_file):
    calib_data = calib_data_file.cols["CalibData2D"]  #1440, 1024
    calib_data_onepixel[row_id, :] = calib_data[pixel_id, 20:990]  # (1000,1440,1024)

calib_data_onepixel_mean= np.mean(calib_data_onepixel, axis=0)

plt.figure()
plt.plot(calib_data_onepixel_mean,'.:')
plt.xlabel("cell id")
plt.ylabel("calibration pedestal data [mV]")
plt.title("calibration pedestal data of a random pixel by linear model")
plt.show()