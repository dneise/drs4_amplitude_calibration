# coding: utf-8
""" This script tries to download drs.fits.gz files which can be used 
    to apply the DRS4 amplitude calibration on the FACT faw-data files (fits.fz)
    in this folder.

    In order to *find* the right drs.fits.gz for each file, I use a TRICK:
    On the FACT run DB website: http://fact-project.org/run_db/db/fact_runinfo.php
    I checked for one of this files in thisfolder, just one, I checked what is the
    run-number difference between the raw-files in this folder and the correct
    associated calibration file. 
    The difference is 1, so I *assume* there is *always* an associated
    drs-calibration file with a run-number one less, than the run number of the files in 
    this folder.
"""
import MySQLdb
import fact
from fact.slowdata import connect
import numpy as np
import h5py
import subprocess
import shlex


class Run(object):
    def __init__(self, night_id, run_id):
        self.night_id = night_id
        self.run_id = run_id

    def __repr__(self):
        return "{!s}({!r})".format(self.__class__.__name__, self.__dict__)

    def __eq__( self, other ):
        return self.night_id == other.night_id and self.run_id == other.run_id

    def __hash__(self):
        return hash(self.night_id) ^ hash(self.run_id)

    def make_isdc_path(self, extension='.fits.fz'):
        """ return a valid path at the ISDC computing cluster in Geneva, where
            the FACT rawdata file with this (night_id, run_id) combination can be found
        """
        night = self.night_id
        year = str(night)[0:4]
        month = str(night)[4:6]
        day = str(night)[6:8]
        return ('/fact/raw/{0}/{1}/{2}/{0}{1}{2}_{3:03d}{4}').format(
                year, 
                month, 
                day, 
                self.run_id,
                extension,
                )
        
h5_overview_file = h5py.File('overview.h5')
night_id = h5_overview_file['night_id'][...]
run_id = h5_overview_file['run_id'][...]

for i in range(len(night_id)):
    r = Run(night_id[i], run_id[i]-1)
    path = r.make_isdc_path(extension='.drs.fits.gz')
    cmd = 'scp isdc-nx:{0} /scratch_loc/comparison_data/.'.format(path)
    call = shlex.split(cmd)
    print i, cmd
    try:
        subprocess.check_call(call)
    except subprocess.CalledProcessError as e:
        print e
 
