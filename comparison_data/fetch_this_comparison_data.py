# coding: utf-8
""" Explain what's going on here!
"""
import MySQLdb
import fact
from fact.slowdata import connect
import numpy as np
import h5py
import subprocess
import shlex


class Run(object):
    def __init__(self, night_id, run_id):
        self.night_id = night_id
        self.run_id = run_id

    def __repr__(self):
        return "{!s}({!r})".format(self.__class__.__name__, self.__dict__)

    def __eq__( self, other ):
        return self.night_id == other.night_id and self.run_id == other.run_id

    def __hash__(self):
        return hash(self.night_id) ^ hash(self.run_id)




run_db = MySQLdb.connect(host="129.194.168.95", # your host, usually localhost
                     user="factread", # your username
                      passwd="r3adfac!", # your password
                      db="factdata") # name of the data base
run_db_cursor = run_db.cursor() 
query = 'SELECT fNight,fRunId,fRunStart,fRunStop FROM RunInfo WHERE fRunTypeKEY=3 AND fDrsStep IS NULL'
print "Executing MySQL query on FACT run DB:"
print "    ", query
number_of_runs_from_1st_query = run_db_cursor.execute(query)
print "number of rows found by query:", number_of_runs_from_1st_query

slowdata_db = connect()

print "trying to fetch temperatures for all these results ... (might take a while)..."
results = []
for i, r in enumerate(run_db_cursor.fetchall()):
    # r is a tuple, but I want to store the results 
    # of the query as *named* attributes, so I can understand what they mean
    s = Run(night_id=r[0], run_id=r[1])
    s.start_time = fact.fjd(r[2])
    s.stop_time = fact.fjd(r[3])
    
    # Now I use start_time and stop_time to retrieve the DRS4-chip temperature 
    # of each run, I just found in the run_DB.
    # I will can a few temperature measurements, between start_time and stop_time ... say 10 or 15.
    # Of these 10 or 15 temperature measurements, I will calculate the mean and store it 
    # by the name "mean_temp"

    # It can happen, that I don't find any temperature between start_time and stop_time, for various reasons
    # In that case I just do not append this result to the results list.

    try:
        slowdata_db_cursor = slowdata_db.FAD_CONTROL_TEMPERATURE.from_until(s.start_time, s.stop_time)
        temp = np.array(slowdata_db_cursor)['temp']
        mean_temp = temp.mean(axis=0)
        s.mean_temp = mean_temp
        
    except (IndexError, ValueError) as e:
        # We get an Index Error, when there ewas nothing between start_time and stop_time in the slowdata_DB
        # And weget an ValueError, when the documents that were found in the slowdata_db, do not have a field called "temp"
        # Both errors goback to the factg, that the slowdata DB is not correctly filled ... this can happen, so we have
        # to deal with it.
        continue
    

    # It can happen, that the DRS4 temperatures, which were stored in the slowdata_DB, were not stored for all 160
    # DRS4 chips, but just the min and max value was stored for each of the 40 boards, in addition the overall min and max 
    # were stored. So it can happen, that we have 82 temperatures instead of 160. 
    # We do not want to work with runs, where we do not have temperatures for all 160 DRS4 chips, so in
    # this case we just do not append this result to the results list
    if s.mean_temp.shape[0] != 160:
        continue

    # for simplicity, we also calculate the overall mean of all temperatures, 
    # using this overall_mean_temperature, we can sort the runs, by temperature. 
    s.overall_mean_temperature = s.mean_temp.mean()


    results.append(s)

print "Of the ", number_of_runs_from_1st_query, "runs we got from the run_DB only", len(results), "met our quality contraints."

temps = np.zeros(len(results))
for i,r in enumerate(results):
    temps[i] = r.overall_mean_temperature


print "The minimum overall_mean_temperature is:", temps.min()
print "The maximum overall_mean_temperature is:", temps.max()

# Of all the resulting runs we still have in hand, I want to find those, whose overall_mean_temperature
# is nearest to every 0.1 degree step between the minimum and maximum overall temperature.

# We use a set here, because, we don't want to have any run twice
runs_to_be_used_for_calibration_model_evaluation = set()

temp_steps = np.arange(temps.min(), temps.max()+0.1, 0.1)
for temp_step in temp_steps:
    index_of_nearest_result = np.argmin(np.abs(temps-temp_step))
    runs_to_be_used_for_calibration_model_evaluation.add(results[index_of_nearest_result])

print "Of all teh runs, that met our quality contraints, we found ", len(runs_to_be_used_for_calibration_model_evaluation), 
print "run, which we will use for our DRS4 amplitude calibration model evaluation."

def make_isdc_path(run_object):
    """ return a valid path at the ISDC computing cluster in Geneva, where
        the FACT rawdata file with this (night_id, run_id) combination can be found
    """
    night = run_object.night_id
    year = str(night)[0:4]
    month = str(night)[4:6]
    day = str(night)[6:8]
    return '/fact/raw/{0}/{1}/{2}/{0}{1}{2}_{3:03d}.fits.fz'.format(year, month, day, run_object.run_id)
    

    
for i, r in enumerate(runs_to_be_used_for_calibration_model_evaluation):
    cmd = 'scp isdc-nx:{0} /scratch_loc/comparison_data/.'.format(make_isdc_path(r))
    call = shlex.split(cmd)
    print i, cmd
    try:
        subprocess.check_call(call)
    except subprocess.CalledProcessError as e:
        print e
       
h5file = h5py.File('/scratch_loc/comparison_data/overview.h5')
night_ids = np.zeros( len(runs_to_be_used_for_calibration_model_evaluation), dtype=np.int32)
run_ids = np.zeros( len(runs_to_be_used_for_calibration_model_evaluation), dtype=np.int32)
start_times = np.zeros( len(runs_to_be_used_for_calibration_model_evaluation), dtype=np.float64)
stop_times = np.zeros( len(runs_to_be_used_for_calibration_model_evaluation), dtype=np.float64)
temperatures = np.zeros( (len(runs_to_be_used_for_calibration_model_evaluation), 160), dtype=np.float32)
for i,g in enumerate(runs_to_be_used_for_calibration_model_evaluation):
    night_ids[i] = g.night_id
    run_ids[i] = g.run_id
    start_times[i] = g.start_time
    stop_times[i] = g.stop_time
    temperatures[i] = g.mean_temp
   
h5file.create_dataset("night_id", data=night_ids)
h5file.create_dataset("run_id", data=run_ids)
h5file.create_dataset("start_time", data=start_times)
h5file.create_dataset("stop_time", data=stop_times)
h5file.create_dataset("temperature", data=temperatures)
h5file.close()
